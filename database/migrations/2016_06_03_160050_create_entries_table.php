<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entries', function (Blueprint $table) {
			$table->increments('id');
			$table->string('euci', 45);
			$table->integer('user_id')->unsigned();
			$table->string('consent', 100)->nullable();
            $table->longText('survey')->nullable();
            $table->longText('interview')->nullable();
            $table->dateTime('consent_completed')->nullable();
            $table->dateTime('survey_completed')->nullable();
            $table->dateTime('interview_completed')->nullable();
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users');
			$table->index('euci');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entries');
	}
}
