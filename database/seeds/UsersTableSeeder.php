<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Bongwook Lee',
            'email' => 'bongwook_lee@abtassoc.com',
            'password' => bcrypt('S0norama'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Michael Costa',
            'email' => 'michael_costa@abtassoc.com',
            'password' => bcrypt('c@lmSystem27'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Diane Fraser',
            'email' => 'diane_fraser@abtassoc.com',
            'password' => bcrypt('richC@nary49'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Lawrence Reichle',
            'email' => 'lawrence_reichle@abtassoc.com',
            'password' => bcrypt('wiseGorill@88'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Jenn Brann',
            'email' => 'jenn_brann@abtassoc.com',
            'password' => bcrypt('ivoryMa$s82'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Vulnerability Scan',
            'email' => 'scan@abtassoc.com',
            'password' => bcrypt('swee+Cat75'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Thuan Huynh',
            'email' => 'Thuan_Huynh@abtassoc.com',
            'password' => bcrypt('lazyR!ng68'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Douglas Fuller',
            'email' => 'Douglas_Fuller@abtassoc.com',
            'password' => bcrypt('sp!cySugar18'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Phomdaen Souvanna',
            'email' => 'Phomdaen_Souvanna@abtassoc.com',
            'password' => bcrypt('roundCopp3r73'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Mariah Sanguinetti',
            'email' => 'Mariah_Sanguinetti@abtassoc.com',
            'password' => bcrypt('goodFlow3r52'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Yvonne Cristy',
            'email' => 'Yvonne_Cristy@abtassoc.com',
            'password' => bcrypt('muddyHou$e99'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'name' => 'Sonja Richard',
            'email' => 'Sonja_Richard@abtassoc.com',
            'password' => bcrypt('mu$hyJelly57'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
    }
}
