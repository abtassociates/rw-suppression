<?php

namespace App\Http\Middleware;

use App\Models\Entry;
use Closure;
use Illuminate\Support\Facades\Route;

class Consent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $params = Route::getFacadeRoot()->current()->parameters();
        $id = isset($params['survey']) ? $params['survey'] : @$params['id'];
        $entry = Entry::findOrFail($id);

        if (empty($entry->consent_completed)) {
            return redirect()->route('consent.get', $entry->id);
        }

        return $next($request);
    }
}
