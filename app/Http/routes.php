<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('survey');
    } else {
        return redirect('login');
    }
});

Route::auth();

Route::resource('survey', 'EntryController');
Route::get('export', 'EntryController@export');

Route::get('interview/{id}', 'EntryController@getInterview')->name('interview.get');
Route::put('interview/{id}', 'EntryController@saveInterview')->name('interview.save');

Route::get('consent/{id}', 'EntryController@getConsent')->name('consent.get');
Route::put('consent/{id}', 'EntryController@saveConsent')->name('consent.save');
