<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use App\StringFormat;
use Illuminate\Http\Request;
use Excel;

class EntryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('consent')->only('edit', 'update', 'getInterview', 'saveInterview');
    }

    public function index()
    {
        $entries = Entry::where('user_id', auth()->user()->id)->get();
        return view('home', ['entries' => $entries]);
    }

    public function create()
    {
        return view('euci');
    }

    public function store(Request $request)
    {
        $rules = [
            'client_first1' => 'required',
            'client_last1' => 'required',
            'client_dob1' => 'required|date',
            'client_gender1' => 'required'
        ];
        $this->validate($request, $rules);

        // store data
        $data = [
            'euci' => $this->deDupUci($this->getUci($request->only(array_keys($rules)), true)),
            'user_id' => auth()->user()->id,
            'survey' => json_encode(['s_dem_age' => $this->getAge($request->input('client_dob1'))]),
        ];
        $entry = Entry::create($data);

        return redirect()->route('consent.get', $entry->id);
    }

    public function edit(Request $request, $id)
    {
        // the given user has to own the entry and the entry must be incomplete.
        $entry = Entry::where('user_id', auth()->user()->id)->where('id', $id)->whereNull('survey_completed')->firstOrFail();

        return view('survey', [
            'id' => $entry->id,
            'euci' => $entry->euci,
            'data' => json_decode($entry->survey)
        ]);
    }

    public function update(Request $request, $id)
    {
        // the given user has to own the entry and the entry must be incomplete to be modified.
        $entry = Entry::where('user_id', auth()->user()->id)->where('id', $id)->whereNull('survey_completed')->firstOrFail();

        // completion check
        $completed = strcasecmp($request->get('action'), 'Finish and Submit') === 0;

        // normalize data
        $input = $request->except(['_token', '_method', 'action']);

        // template for return data
        $return = [
            'id' => $id,
            'success' => false,
            'message' => '',
            'interview_button' => $completed,
        ];

        // update
        try {
            $entry->update([
                'survey' => json_encode($input),
                'survey_completed' => ($completed ? time() : null),
            ]);

            $return['success'] = true;
            $return['message'] = sprintf(
                'Your survey <string>(#%s)</string> has been successfully %s.',
                $entry->id,
                ($completed ? 'finalized and submitted' : 'updated and saved as pending')
            );
        } catch (Exception $e) {
            $return['message'] = sprintf('There was an error <strong>(#%s)</strong>. Please try again.', $e->getCode());
        }

        return view('result')->with(['data' => $return]);
    }

    public function getInterview(Request $request, $id)
    {
        // the given user has to own the entry and the entry must be incomplete.
        $entry = Entry::where('user_id', auth()->user()->id)->where('id', $id)->whereNull('interview_completed')->firstOrFail();

        // pull survey data
        $c1q = [
            's_stg_1a' => 'Felt having HIV was a punishment for things you had done in the past',
            's_stg_1b' => 'Felt that people were avoiding you because of your HIV status',
            's_stg_1c' => 'Feared that you would lose your friends if they learned about your HIV status',
            's_stg_1d' => 'Felt like people that you know were treating you differently because of your HIV status',
            's_stg_1e' => 'Felt like people looked down on you because you have HIV',
            's_stg_1f' => 'Avoided dating because you believed most people do not want a relationship with someone with HIV',
            's_stg_1g' => 'Avoided a situation because you were worried about people knowing you have HIV',
            's_stg_1h' => 'Been embarrassed about having HIV',
        ];
        $c1a = ['Refused to answer', 'Never', 'Rarely', 'Sometimes', 'Often', 'Always'];
        $c1r = [];
        $c2q = [
            's_stg_2a' => 'Acted uncomfortable with you?',
            's_stg_2b' => 'Treated you as an inferior?',
            's_stg_2c' => 'Preferred to avoid you?',
            's_stg_2d' => 'Refused you service?',
        ];
        $c2r = [];

        $survey = json_decode($entry->survey, true);
        if (is_array($survey)) {
            foreach ($survey as $k => $v) {
                if (preg_match('/^s_stg_1[a-h]$/', $k) && $v >= 3) {
                    $c1r[$k] = $v;
                } elseif (preg_match('/^s_stg_2[a-d]$/', $k) && strcmp($v, 'Yes') === 0) {
                    $c2r[$k] = $v;
                }
            }
        }

        return view('interview', [
            'id' => $entry->id,
            'euci' => $entry->euci,
            'data' => json_decode($entry->interview),
            'c1q' => $c1q,
            'c1a' => $c1a,
            'c1r' => $c1r,
            'c2q' => $c2q,
            'c2r' => $c2r,
        ]);
    }

    public function saveInterview(Request $request, $id)
    {
        // the given user has to own the entry and the entry must be incomplete to be modified.
        $entry = Entry::where('user_id', auth()->user()->id)->where('id', $id)->whereNull('interview_completed')->firstOrFail();

        // completion check
        $completed = strcasecmp($request->get('action'), 'Finish and Submit') === 0;

        // normalize data
        $input = $request->except(['_token', '_method', 'action']);

        // template for return data
        $return = [
            'id' => $id,
            'success' => false,
            'message' => '',
            'interview_button' => false,
        ];

        // update
        try {
            $entry->update([
                'interview' => json_encode($input),
                'interview_completed' => ($completed ? time() : null),
            ]);

            $return['success'] = true;
            $return['message'] = sprintf(
                'Your interview <string>(#%s)</string> has been successfully %s.',
                $entry->id,
                ($completed ? 'finalized and submitted' : 'updated and saved as pending')
            );
        } catch (Exception $e) {
            $return['message'] = sprintf('There was an error <strong>(#%s)</strong>. Please try again.', $e->getCode());
        }

        return view('result')->with(['data' => $return]);
    }

    public function getConsent(Request $request, $id)
    {
        // the given user has to own the entry and the entry must be incomplete to be modified.
        $entry = Entry::where('user_id', auth()->user()->id)->where('id', $id)->whereNull('consent_completed')->firstOrFail();

        return view('consent', [
            'id' => $entry->id,
            'euci' => $entry->euci,
            'timestamp' => date('r'),
        ]);
    }

    public function saveConsent(Request $request, $id)
    {
        // the given user has to own the entry and the entry must be incomplete to be modified.
        $entry = Entry::where('user_id', auth()->user()->id)->where('id', $id)->whereNull('consent_completed')->firstOrFail();

        // validate data
        $this->validate($request, ['interviewer_signature' => 'required']);

        // update
        try {
            $entry->update([
                'consent' => $request->get('interviewer_signature'),
                'consent_completed' => strtotime($request->get('c_time', date('r'))),
            ]);
            return redirect()->route('survey.edit', $entry->id);
        } catch (Exception $e) {
            return redirect()->back()->withErrors([sprintf('There was an error <strong>(#%s)</strong>. Please try again.', $e->getCode())]);
        }
    }

    private function getUci($data, $encrypt = false)
    {
        // clean up
        foreach ($data as $k => $v) {
            $data[$k] = StringFormat::removeAccents(trim($v));
        }

        // format
        $first = str_pad($data['client_first1'], 2, '9');
        $last = str_pad($data['client_last1'], 2, '9');
        list($m, $d, $y) = explode('/', date_create($data['client_dob1'])->format('m/d/y'));
        $gender = $data['client_gender1'];

        // generate
        $uic = strtoupper($first . $last . $m . $d . $y . $gender);

        return $encrypt ? strtoupper(sha1($uic)) : $uic;
    }

    private function deDupUci($euci)
    {
        $entries = Entry::where('euci', 'LIKE', "${euci}_")->get();

        if ($entries->count() == 0) {
            $newId = $euci . 'U';
        } elseif ($entries->count() == 1) {
            $entries->first()->timestamps = false;
            $entries->first()->update(['euci' => $euci . 'A']);
            $newId = $euci . 'B';
        } else {
            $newId = $euci . range('A', 'Z')[$entries->count()];
        }

        return $newId;
    }

    public function export()
    {
        $fileName = sprintf('suppression_%s', date('YmdHis'));
        Excel::create($fileName, function ($excel) {
            $excel->sheet('Surveys', function ($sheet) {
                $sheet->setOrientation('landscape');
                $sheet->freezeFirstRow();

                $data = $this->getExportData('survey');

                $rowNum = 1;
                $sheet->row($rowNum++, $data['cols']);
                foreach ($data['rows'] as $r) {
                    $sheet->row($rowNum++, $r);
                }
            });
            $excel->sheet('Interviews', function ($sheet) {
                $sheet->setOrientation('landscape');
                $sheet->freezeFirstRow();

                $data = $this->getExportData('interview');

                $rowNum = 1;
                $sheet->row($rowNum++, $data['cols']);
                foreach ($data['rows'] as $r) {
                    $sheet->row($rowNum++, $r);
                }
            });
        })->store('xlsx');
    }

    private function getExportData($which)
    {
        $data = array(
            'cols' => array(),
            'rows' => array(),
            'meta' => array(),
        );

        // extract data and gather all unique data table columns
        foreach (Entry::whereNotNull("{$which}_completed")->get() as $entry) {
            $row = json_decode($entry->{$which}, true);
            $data['cols'] = array_unique(array_merge($data['cols'], array_keys($row)));
            $data['rows'][] = $row;
            $data['meta'][] = array(
                'entry_id' => $entry->id,
                'euci' => $entry->euci,
                'created_at' => $entry->created_at,
                'updated_at' => $entry->updated_at
            );
        }
        sort($data['cols']);
        array_unshift($data['cols'], 'entry_id', 'euci', 'created_at', 'updated_at');

        // fill in missing column data with empties
        for ($i = 0; $i < count($data['rows']); $i++) {
            foreach ($data['cols'] as $c) {
                if (!array_key_exists($c, $data['rows'][$i])) {
                    $data['rows'][$i][$c] = '';
                }
                if (is_array($data['rows'][$i][$c])) {
                    $data['rows'][$i][$c] = join('|', $data['rows'][$i][$c]);
                }
            }
            ksort($data['rows'][$i]);
            $data['rows'][$i] = $data['meta'][$i] + $data['rows'][$i];
        }

        return $data;
    }

    private function getAge($birthday)
    {
        $dob = date_create($birthday);
        return is_null($dob) ? null : date_diff($dob, date_create('today'))->y;
    }
}
