<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    const STATUS_NEW = 0;
    const STATUS_PENDING = 1;
    const STATUS_COMPLETE = 2;

    protected $fillable = ['euci', 'user_id', 'consent', 'survey', 'interview', 'consent_completed', 'survey_completed', 'interview_completed'];
    protected $dates = ['consent_completed', 'survey_completed', 'interview_completed', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getStatus($section)
    {
        if (!is_null($this->{"${section}_completed"})) {
            return self::STATUS_COMPLETE;
        } elseif (is_null($this->{"${section}_completed"}) && !is_null($this->{$section})) {
            return self::STATUS_PENDING;
        }

        return self::STATUS_NEW;
    }
}
