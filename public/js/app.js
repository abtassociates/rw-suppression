jQuery(document).ready(function ($) {

	// form navigation buttons
	$('.form-nav .previous a').click(function (e) {
		e.preventDefault();
		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
		$('html, body').animate({scrollTop: 0}, 300);
	});
	$('.form-nav .next a').click(function (e) {
		e.preventDefault();
		$('.nav-tabs > .active').next('li').find('a').trigger('click');
		$('html, body').animate({scrollTop: 0}, 300);
	});

	// form validation
	$.validator.setDefaults({
		ignore: []
	});
	$('#euci, #consent, #survey, #interview').validate({
		invalidHandler: function (event, validator) {
			if (validator.numberOfInvalids() > 0) {
				$('.nav-tabs').children('li').first().find('a').trigger('click');
			}
		},
		errorPlacement: function (error, element) {
			element.closest('div.form-group').children('label').first().after(error);
		},
		submitHandler: function (form) {
			var id = $(form).attr('id');
			var confirm = $(form).data('confirm');

			if (confirm) {
				if (id === 'euci') {
					var first = $(form).find('input[name="client_first1"]').val();
					var last = $(form).find('input[name="client_last1"]').val();
					var dob = $(form).find('input[name="client_dob1"]').val();
					var gender = $(form).find('input[name="client_gender1"]:checked').closest('label').text().trim();

					$('#ph-first').text(first);
					$('#ph-last').text(last);
					$('#ph-dob').text(moment(dob).format('MM/DD/YYYY'));
					$('#ph-gender').text(gender);
				}

				$('#modal-' + id).modal();
			} else {
				form.submit();
			}
		}
	});
	$('.modal-ok, .modal-skip').click(function (e) {
		e.preventDefault();

		var target = '#' + (
			$(this).hasClass('modal-ok')
				? $(this).closest('.modal').data('target')
				: $(this).closest('form').attr('id')
		);

		$(target).data('confirm', 0).submit();
	});

	// applies to all collection form elements
	$('#survey')
		// prevent enter/return key from submitting the form
		.on('keyup keypress', function (e) {
			var keyCode = e.keyCode || e.which;
			if (keyCode === 13) {
				e.preventDefault();
				return false;
			}
		})
		// clears radio button block
		.on('click', '.btn-radio-clear', function (e) {
			e.preventDefault();
			e.stopPropagation();

			$(this).closest('.form-group').find(':input').each(function () {
				$(this).clearField();
			});

			if ($(this).hasClass('cbr')) {
				cbr();
			}
		});

	// branching
	$('.br').click(function (e) {
		var toggle = $(this).data('toggle');
		var target = '.' + $(this).data('target');

		branching(toggle, target);
	});

	$('.cbr').click(function (e) {
		cbr();
	});

	var cbr = function () {
		var selector = '[name^="s_stg_2"]:checked';
		var target = '.cbr-hcp';
		branchingBySelector(selector, target);
	};

	// helper: toggles target section and clears input when closed
	var branchingBySelector = function (selector, target) {
		var toggle = false;
		$(selector).each(function () {
			if ($(this).val() == 'Yes') {
				toggle = true;
				return false;
			}
		});
		branching(toggle, target);
	};
	var branching = function (toggle, target) {
		if (toggle) {
			$(target).slideDown();
		} else {
			// cascade if there's a child node
			var subTarget = '';
			$(target).find(':input').each(function () {
				if ($(this).data('target')) {
					subTarget = '.' + $(this).data('target');
					return false;
				}
			});
			if (subTarget.length > 0) {
				branching(0, subTarget);
			}

			// clear target fields so that the data doesn't make it to the server
			$(target).slideUp().find(':input').each(function () {
				$(this).clearField();
			});
		}
	};

	// helper: clears the given form element
	$.fn.clearField = function () {
		var tagName = this.prop('tagName').toLowerCase();

		if (tagName == 'select') {
			$(this).removeAttr('selected');
			$(this).val('');
		} else if (tagName == 'input') {
			if ($.inArray($(this).attr('type').toLowerCase(), ['checkbox', 'radio']) > -1) {
				$(this).removeAttr('checked');
			} else {
				$(this).val('');
			}
		} else {
			$(this).val('');
		}

		return this;
	};

});