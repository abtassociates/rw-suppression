@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">New Chart Abstraction Form</div>
                    <div class="panel-body">
                        <form id="euci" action="{{ route('survey.store') }}" method="post" data-confirm="1">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>First and third letters of first name:</label>
                                    @if ($errors->has('client_first1'))
                                        <label id="client_first1-error" class="error" for="client_first1">This field is required.</label>
                                    @endif
                                    <input type="text" class="form-control" minlength="1" maxlength="2" required name="client_first1" data-name="client_first">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>First and third letters of last name:</label>
                                    @if ($errors->has('client_last1'))
                                        <label id="client_last1-error" class="error" for="client_last1">This field is required.</label>
                                    @endif
                                    <input type="text" class="form-control" minlength="1" maxlength="2" required name="client_last1" data-name="client_last">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Date of birth: <small>Enter January 1 if month and date are unknown</small></label>
                                    @if ($errors->has('client_dob1'))
                                        <label id="client_dob1-error" class="error" for="client_dob1">This field is required.</label>
                                    @endif
                                    <input type="date" class="form-control" required name="client_dob1" data-name="client_dob">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Current Gender:</label>
                                    @if ($errors->has('client_gender1'))
                                        <label id="client_gender1-error" class="error" for="client_gender1">This field is required.</label>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="radio" style="margin:0;">
                                                <label>
                                                    <input type="radio" name="client_gender1" data-name="client_gender" value="1" required>
                                                    Male
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="radio" style="margin:0;">
                                                <label>
                                                    <input type="radio" name="client_gender1" data-name="client_gender" value="2">
                                                    Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="radio" style="margin:0;">
                                                <label>
                                                    <input type="radio" name="client_gender1" data-name="client_gender" value="3">
                                                    Transgender
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="radio" style="margin:0;">
                                                <label>
                                                    <input type="radio" name="client_gender1" data-name="client_gender" value="9">
                                                    Unknown
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <nav class="form-nav">
                                <ul class="pager">
                                    <li class="pull-left"><a href="{{ route('survey.index') }}">Cancel</a></li>
                                    <li class="next pull-right"><input type="submit" class="btn btn-primary" value="Next &rarr;"></li>
                                </ul>
                            </nav>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-euci" class="modal fade" tabindex="-1" role="dialog" data-target="euci">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <p>Please confirm these data are correct before proceeding.</p>

                    <div class="well">
                        <div class="row">
                            <div class="col-sm-6">
                                First and third letters of first name: <strong id="ph-first"></strong>
                            </div>
                            <div class="col-sm-6">
                                First and third letters of last name: <strong id="ph-last"></strong>
                            </div>
                            <div class="col-sm-6">
                                Date of birth: <strong id="ph-dob"></strong>
                            </div>
                            <div class="col-sm-6">
                                Current Gender: <strong id="ph-gender"></strong>
                            </div>
                        </div>
                    </div>

                    <p>
                        Click <strong class="text-primary">OK</strong> to initiate Form with these data
                        or <strong class="text-danger">Cancel</strong> to edit.
                        You will not be able to change this information after clicking <strong class="text-primary">OK</strong>.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary modal-ok">OK</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
