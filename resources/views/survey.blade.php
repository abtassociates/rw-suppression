@extends('layouts.app')

@section('content')
    <nav>
        <ul id="nav-main" class="nav nav-tabs nav-justified" role="tablist">
            <li role="presentation" class="active">
                <a href="#tab-pane1" aria-controls="tab-pane1" role="tab" data-toggle="tab">Introduction</a>
            </li>
            <li role="presentation">
                <a href="#tab-pane2" aria-controls="tab-pane2" role="tab" data-toggle="tab">Demographics</a>
            </li>
            <li role="presentation">
                <a href="#tab-pane3" aria-controls="tab-pane3" role="tab" data-toggle="tab">Access to Care and Health Care Coverage</a>
            </li>
            <li role="presentation">
                <a href="#tab-pane4" aria-controls="tab-pane4" role="tab" data-toggle="tab">Stigma</a>
            </li>
            <li role="presentation">
                <a href="#tab-pane5" aria-controls="tab-pane5" role="tab" data-toggle="tab">Medical Mistrust</a>
            </li>
            <li role="presentation">
                <a href="#tab-pane6" aria-controls="tab-pane6" role="tab" data-toggle="tab">Trauma History and Current Exposure</a>
            </li>
            <li role="presentation">
                <a href="#tab-pane7" aria-controls="tab-pane7" role="tab" data-toggle="tab">Coping Self-Efficacy</a>
            </li>
            <li role="presentation">
                <a href="#tab-pane8" aria-controls="tab-pane8" role="tab" data-toggle="tab">Case Adherence Scale</a>
            </li>
            <li role="presentation">
                <a href="#tab-pane9" aria-controls="tab-pane9" role="tab" data-toggle="tab">Substance Use</a>
            </li>
            <li role="presentation">
                <a href="#tab-pane10" aria-controls="tab-pane10" role="tab" data-toggle="tab">Closing</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <form id="survey" action="{{ route('survey.update', $id) }}" method="post" data-confirm="1">
            <input name="_method" type="hidden" value="PUT">
            {!! csrf_field() !!}
            <div class="tab-content">

                <div id="id-strip" class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div id="id-strip-ucid" class="col-sm-12 col-md-7">Unique Client ID: <strong>{{ $euci }}</strong></div>
                            <div id="id-strip-review-period" class="col-sm-12 col-md-5">
                                OMB Number: <strong>0906-0033</strong>
                                &nbsp; &nbsp;
                                Expiration Date: <strong>12-31-2020</strong>
                            </div>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane active" id="tab-pane1">
                    @include('includes.s-intro')
                    <nav class="form-nav">
                        <ul class="pager">
                            <li class="next"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div role="tabpanel" class="tab-pane" id="tab-pane2">
                    @include('includes.s-demographics')
                    <nav class="form-nav">
                        <ul class="pager">
                            <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                            <li><input type="submit" class="btn btn-secondary modal-skip" formnovalidate="formnovalidate" name="action" value="Save as Pending"></li>
                            <li class="next"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div role="tabpanel" class="tab-pane" id="tab-pane3">
                    @include('includes.s-access')
                    <nav class="form-nav">
                        <ul class="pager">
                            <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                            <li><input type="submit" class="btn btn-secondary modal-skip" formnovalidate="formnovalidate" name="action" value="Save as Pending"></li>
                            <li class="next"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div role="tabpanel" class="tab-pane" id="tab-pane4">
                    @include('includes.s-stigma')
                    <nav class="form-nav">
                        <ul class="pager">
                            <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                            <li><input type="submit" class="btn btn-secondary modal-skip" formnovalidate="formnovalidate" name="action" value="Save as Pending"></li>
                            <li class="next"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div role="tabpanel" class="tab-pane" id="tab-pane5">
                    @include('includes.s-medical')
                    <nav class="form-nav">
                        <ul class="pager">
                            <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                            <li><input type="submit" class="btn btn-secondary modal-skip" formnovalidate="formnovalidate" name="action" value="Save as Pending"></li>
                            <li class="next"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div role="tabpanel" class="tab-pane" id="tab-pane6">
                    @include('includes.s-trauma')
                    <nav class="form-nav">
                        <ul class="pager">
                            <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                            <li><input type="submit" class="btn btn-secondary modal-skip" formnovalidate="formnovalidate" name="action" value="Save as Pending"></li>
                            <li class="next"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div role="tabpanel" class="tab-pane" id="tab-pane7">
                    @include('includes.s-coping')
                    <nav class="form-nav">
                        <ul class="pager">
                            <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                            <li><input type="submit" class="btn btn-secondary modal-skip" formnovalidate="formnovalidate" name="action" value="Save as Pending"></li>
                            <li class="next"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div role="tabpanel" class="tab-pane" id="tab-pane8">
                    @include('includes.s-case')
                    <nav class="form-nav">
                        <ul class="pager">
                            <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                            <li><input type="submit" class="btn btn-secondary modal-skip" formnovalidate="formnovalidate" name="action" value="Save as Pending"></li>
                            <li class="next"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div role="tabpanel" class="tab-pane" id="tab-pane9">
                    @include('includes.s-substance')
                    <nav class="form-nav">
                        <ul class="pager">
                            <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                            <li><input type="submit" class="btn btn-secondary modal-skip" formnovalidate="formnovalidate" name="action" value="Save as Pending"></li>
                            <li class="next"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div role="tabpanel" class="tab-pane" id="tab-pane10">
                    @include('includes.s-closing')
                    <nav class="form-nav">
                        <ul class="pager">
                            <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                            <li><input type="submit" class="btn btn-secondary modal-skip" formnovalidate="formnovalidate" name="action" value="Save as Pending"></li>
                            <li class="pull-right"><input type="submit" class="btn btn-primary" name="action" value="Finish and Submit"></li>
                        </ul>
                    </nav>
                </div>

            </div>
        </form>
    </div>

    <div id="modal-survey" class="modal fade" tabindex="-1" role="dialog" data-target="survey">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <p>
                        Click <strong class="text-primary">OK</strong> to finalize the form
                        or <strong class="text-danger">Cancel</strong> to edit.
                        You will not be able to change this information after clicking <strong class="text-primary">OK</strong>.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary modal-ok">OK</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('footer')
    <div class="container-fluid">
        <div class="well well-sm">Public Burden Statement: An agency may not conduct or sponsor, and a person is not required to respond to, a collection of information unless it displays a currently valid OMB control number.  The OMB control number for this project is 0906-0033.  Public reporting burden for this collection of information is estimated to average 30 minutes per interview. Send comments regarding this burden estimate or any other aspect of this collection of information, including suggestions for reducing this burden, to HRSA Reports Clearance Officer, 5600 Fishers Lane, Room 10-29, Rockville, Maryland, 20857.</div>
    </div>
@endsection

@section('scripts')
    <script>
        jQuery(document).ready(function () {
        	var data = <?php echo json_encode($data); ?>;
	        // populate the form with previous input
	        for (prop in data) {
		        if (data.hasOwnProperty(prop)) {
			        var selector = ':input[name="' + prop + '"], :input[name="' + prop + '[]"]';
			        var tagType = $(selector).attr('type');

			        if (tagType === 'radio' || tagType === 'checkbox') {
				        var responses = Array.isArray(data[prop]) ? data[prop] : [data[prop]];
				        $(selector).filter(function () {
					        return $.inArray(this.value, responses) !== -1;
				        }).prop('checked', true);
			        } else {
				        $(selector).val(data[prop]);
			        }
		        }
	        }

	        // expand sub-sections based on branching logic
	        $('.br:checked, .cbr:checked').trigger('click');
        });
    </script>
@endsection