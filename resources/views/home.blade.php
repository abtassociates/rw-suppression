@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="clearfix" style="margin-bottom:10px;">
                    <h2 class="pull-left">Surveys & Interviews</h2>
                    <h1 class="pull-right"><a class="btn btn-primary" href="{{ route('survey.create') }}">Create New</a></h1>
                </div>

                <div class="table-responsive">
                    <table id="table-entries" class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Last Updated</th>
                            <th>eUCI</th>
                            <th class="text-center">Survey</th>
                            <th class="text-center">Interview</th>
                        </tr>
                        </thead>
                        @if($entries->count())
                            <tbody>
                            @foreach($entries as $e)
                                <?php
                                $surveyStatus = $e->getStatus('survey');
                                $interviewStatus = $e->getStatus('interview');
                                ?>
                                <tr>
                                    <td>{{ $e->id }}</td>
                                    <td>{{ $e->updated_at }}</td>
                                    <td>{{ $e->euci }}</td>
                                    <td class="text-center">
                                        @if ($surveyStatus == \App\Models\Entry::STATUS_COMPLETE)
                                            <strong>Completed</strong>
                                        @else
                                            <a href="{{ route('survey.edit', $e->id) }}" class="btn btn-sm btn-default">
                                                @if ($surveyStatus == \App\Models\Entry::STATUS_PENDING)
                                                    Continue
                                                @else
                                                    Start
                                                @endif
                                            </a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if ($interviewStatus == \App\Models\Entry::STATUS_COMPLETE)
                                            <strong>Completed</strong>
                                        @else
                                            <a href="{{ route('interview.get', $e->id) }}" class="btn btn-sm btn-default">
                                                @if ($interviewStatus == \App\Models\Entry::STATUS_PENDING)
                                                    Continue
                                                @else
                                                    Start
                                                @endif
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        @endif
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Last Updated</th>
                            <th>eUCI</th>
                            <th class="text-center">Survey</th>
                            <th class="text-center">Interview</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script>
        jQuery(document).ready(function ($) {
            $('#table-entries').DataTable({
                'order': [[1, 'desc']]
            });

            $('input').first().css("border","1px solid #ccc");

        });
    </script>
@endsection