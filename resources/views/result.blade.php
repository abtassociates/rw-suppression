@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="alert alert-{{ $data['success'] ? 'success' : 'danger' }}">{!! $data['message'] !!}</div>

        <div class="text-center">
            <a href="{{ route('survey.index') }}" class="btn btn-default">OK</a>
            @if ($data['interview_button'])
                <a href="{{ route('interview.get', $data['id']) }}" class="btn btn-primary" style="margin-left:20px;">Continue to Interview</a>
            @endif
        </div>
    </div>
@endsection
