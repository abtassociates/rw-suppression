<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="form-group col-md-4">
				<label>1. What is your age in years? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<input type="number" class="form-control" name="s_dem_age" min="1" value="{{ @$data->s_dem_age }}">
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>2. What was your sex at birth? <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_dem_sex_birth" value="Male">
						Male
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_birth" value="Female">
						Female
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_birth" value="Intersex/ambiguous">
						Intersex/ambiguous
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_birth" value="Refused to Answer">
						Refused to Answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>3. Do you consider yourself to be male, female, or transgender? <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_dem_sex_considered" value="Male">
						Male
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_considered" value="Female">
						Female
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_considered" value="Transgender">
						Transgender
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_considered" value="Refused to Answer">
						Refused to Answer
					</label>
				</div>
			</div>
		</div>
		<div class="row hr">
			<div class="form-group col-md-4">
				<label>4. Do you think of yourself as: <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_dem_sex_orientation" value="Lesbian or gay">
						Lesbian or gay
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_orientation" value="Bisexual">
						Bisexual
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_orientation" value="Straight, that is, not gay">
						Straight, that is, not gay
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_orientation" value="Something else">
						Something else
					</label>
				</div>
				<div class="indent">
					Please specify:
					<div class="inline">
						<input type="text" class="form-control" name="s_dem_sex_orientation_specify">
					</div>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_orientation" value="Refused to Answer">
						Refused to Answer
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_sex_orientation" value="Don't know">
						Don't know
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>5. The next question is about your current legal marriage status. Are you currently: <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_dem_marital_status" value="Married">
						Married
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_marital_status" value="In a civil union or domestic partnership">
						In a civil union or domestic partnership
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_marital_status" value="Divorced">
						Divorced
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_marital_status" value="Widowed">
						Widowed
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_marital_status" value="Separated">
						Separated
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_marital_status" value="Never married">
						Never married
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_marital_status" value="Refused to answer">
						Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>6. Are you currently living with your husband, wife, boyfriend, girlfriend, or partner? <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_dem_cohabit" value="No">
						No
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_cohabit" value="Yes">
						Yes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_cohabit" value="Refused to Answer">
						Refused to Answer
					</label>
				</div>
			</div>
		</div>
		<div class="row hr">
			<div class="form-group col-md-4">
				<label>7. Do you consider yourself to be Hispanic or Latina? <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_dem_hispanic" value="No, not Hispanic, Latina">
						No, not Hispanic, Latina
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_hispanic" value="Yes, Mexican, Mexican American Chicana">
						Yes, Mexican, Mexican American Chicana
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_hispanic" value="Yes, Puerto Rican">
						Yes, Puerto Rican
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_hispanic" value="Yes, Cuban">
						Yes, Cuban
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_hispanic" value="Yes, another Hispanic, Latina">
						Yes, another Hispanic, Latina
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_hispanic" value="Refused to answer">
						Refused to answer
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_hispanic" value="Don't know">
						Don't know
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>8. What is your race? You may choose more than one option category? (Select all that apply) <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="checkbox" style="margin-top:0;">
					<label>
						<input type="checkbox" name="s_dem_race[]" value="American Indian or Alaska Native">
						American Indian or Alaska Native
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_race[]" value="Asian">
						Asian
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_race[]" value="Black or African-American">
						Black or African-American
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_race[]" value="Native Hawaiian or other Pacific Islander">
						Native Hawaiian or other Pacific Islander
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_race[]" value="White">
						White
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_race[]" value="Refused to answer">
						Refused to answer
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_race[]" value="Don't know">
						Don't know
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>9. What is the highest level of education you have completed? <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_dem_education" value="Never attended school">
						Never attended school
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_education" value="Grades 1 through 8">
						Grades 1 through 8
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_education" value="Grades 9 through 11">
						Grades 9 through 11
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_education" value="Grades 12 or GED">
						Grades 12 or GED
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_education" value="Some college, Associates Degree, or Technical Degree">
						Some college, Associates Degree, or Technical Degree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_education" value="Bachelor's Degree or greater">
						Bachelor's Degree or greater
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_education" value="Refused to answer">
						Refused to answer
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_education" value="Don't know">
						Don't know
					</label>
				</div>
			</div>
		</div>
		<div class="row hr">
			<div class="form-group col-md-4">
				<label>10. In the last year, what was your combined yearly household income from all sources before taxes? When I say "combined household income", I mean the total amount of money from all people living in the household. <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_dem_income" value="0 to $4,999 per year">
						0 to $4,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$5,000 - $9,999 per year">
						$5,000 - $9,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$10,000 - $12,499 per year">
						$10,000 - $12,499 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$12,500 - $14,999 per year">
						$12,500 - $14,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$15,000 - $19,999 per year">
						$15,000 - $19,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$20,000 - $24,999 per year">
						$20,000 - $24,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$25,000 - $29,999 per year">
						$25,000 - $29,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$30,000 - $34,999 per year">
						$30,000 - $34,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$35,000 - $39,999 per year">
						$35,000 - $39,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$40,000 - $49,999 per year">
						$40,000 - $49,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$50,000 - $59,999 per year">
						$50,000 - $59,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$60,000 - $74,999 per year">
						$60,000 - $74,999 per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="$75,000 or more per year">
						$75,000 or more per year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="Refused to answer">
						Refused to answer
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_income" value="Don't know">
						Don't know
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>11. Including you, how many people depend on this income in the past year? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<input type="number" class="form-control" name="s_dem_dependants" min="0">
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_dependants_refused" value="Refused to answer">
						Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>12. Are you currently: <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_dem_employment" value="Employed for wages">
						Employed for wages
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_employment" value="Self-employed">
						Self-employed
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_employment" value="Out of work for more than 1 year">
						Out of work for more than 1 year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_employment" value="Out of work for less than 1 year">
						Out of work for less than 1 year
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_employment" value="A homemaker">
						A homemaker
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_employment" value="A student">
						A student
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_employment" value="Retired">
						Retired
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_employment" value="Unable to work">
						Unable to work
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_employment" value="Refused to Answer">
						Refused to Answer
					</label>
				</div>
			</div>
		</div>
		<div class="row hr">
			<div class="form-group col-md-4">
				<label>13. During the past 12 months, have you done any of the following (select all that apply): <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="checkbox" style="margin-top:0;">
					<label>
						<input type="checkbox" name="s_dem_living_condition[]" value="Lived on the street">
						Lived on the street
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_living_condition[]" value="Lived in a shelter">
						Lived in a shelter
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_living_condition[]" value="Lived in a Single Room Occupancy (SRO) hotel">
						Lived in a Single Room Occupancy (SRO) hotel
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_living_condition[]" value="Lived in a car">
						Lived in a car
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="s_dem_living_condition[]" value="Refused to answer">
						Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>14. How long have you been living at your current address? <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="row">
					<span class="col-sm-2">Year</span>
					<div class="col-sm-3">
						<input type="number" class="form-control" name="s_dem_addr_year" min="0">
					</div>
					<span class="col-sm-2 col-sm-offset-1">Month</span>
					<div class="col-sm-3">
						<input type="number" class="form-control" name="s_dem_addr_month" min="0">
					</div>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_addr_response" value="Refused to answer">
						Refused to answer
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_dem_addr_response" value="Don't know">
						Don't know
					</label>
				</div>
			</div>
		</div>
	</div>
</div>