<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
				<div>
					<p><label>27. Are there any important points that you want to be sure we are aware of, that we did not talk about already?</label></p>

					<p><textarea class="form-control" name="i_clo_27"></textarea></p>
				</div>

				<p>Thank you for your participation, this information will be very helpful to HRSA.</p>
			</div>
		</div>
	</div>
</div>
