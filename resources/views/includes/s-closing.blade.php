<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
				<div>
					<p><label>1. Now, thinking about all the things we discussed here, can you tell me what two things you believe are most important for you in managing your HIV.</label></p>
					<p><em>Probe: explain how these are important to managing your HIV… or give an example of a time when x service helped you better manage your HIV (e.g., through accessing HIV care, treatment adherence, etc.)?</em></p>
					<p><textarea class="form-control" name="s_clo_manage"></textarea></p>
				</div>

				<div>
					<p><label>2. Are there any important points that you want to be sure we are aware of, that we did not talk about already?</label></p>
					<p><textarea class="form-control" name="s_clo_aware"></textarea></p>
				</div>

				<p>Thank you for your participation, this information will be very helpful to HRSA.</p>

				<p>We have finished our interview. On behalf of our project team, thank you very much for your participation today. We appreciate your feedback and that you were willing to share your experience with us. Sometimes questions like these can trigger memories or raise questions about your health. If any of the things we discussed today raised concerns for you about your health, please discuss them with your physician.</p>

				<p>Thank you again.  The information you provided will help HRSA design and fund services to help people with HIV.</p>
			</div>
		</div>
	</div>
</div>
