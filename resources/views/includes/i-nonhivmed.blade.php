<div class="panel panel-default">
	<div class="panel-body">
		<div class="well well-sm text-info"><strong>Next, we are going to discuss medical and other primary care services not necessarily related to HIV care, this could include things like mental health, substance use, hypertension, diabetes, hepatitis, or any other medical issues.</strong></div>

		<div class="row">
			<div class="form-group col-md-6">
				<label>17. Describe the types of non-HIV medical services you regularly get at this clinic or elsewhere <u>within the past year</u>. Do you receive most of these services here? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_nhivm_17" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>18. Which non-HIV medical services do you feel are <u>most important</u> in helping manage your HIV? Why? <span class="label label-default btn-radio-clear">clear</span></label>
				<p><small>Probe: consider how other medical services affect the ability to achieve or maintain viral suppression?</small></p>
				<div>
					<textarea name="i_nhivm_18" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>19. Next, I am going to ask questions related to specific barriers or challenges that you may or may not have experienced in trying to access <u>non-HIV medical care</u>. We are particularly interested in learning how these barriers or challenges affect your ability to achieve and/or maintain viral suppression. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_nhivm_19" class="form-control"></textarea>
				</div>
			</div>
		</div>

		@if(count($c1r) || count($c2r))
			<div class="row hr">
				@if(count($c1r))
					<div class="form-group col-md-6">
						<label>19.a. You mentioned that in the past year you experienced:</label>
						<div class="alert alert-info" style="margin-bottom:0;">
							<ul>
								@foreach($c1r as $k => $v)
									<li>{{ $c1q[$k] }}: <strong>{{ $c1a[$v] }}</strong></li>
								@endforeach
							</ul>
						</div>
						<label>Could you describe how that/those experience(s) has impacted your ability and/or willingness to access non-HIV medical care? <span class="label label-default btn-radio-clear">clear</span></label>
						<div>
							<textarea name="i_nhivm_19a" class="form-control"></textarea>
						</div>
					</div>
				@endif
				@if(count($c2r))
					<div class="form-group col-md-6">
						<label>19.b. You mentioned that since you have had HIV, you experienced:</label>
						<div class="alert alert-info" style="margin-bottom:0;">
							<ul>
								@foreach($c2r as $k => $v)
									<li>{{ $c2q[$k] }} <strong>{{ $v }}</strong></li>
								@endforeach
							</ul>
						</div>
						<label>Could you describe how that experience has impacted your ability and/or willingness to access non-HIV medical care? <span class="label label-default btn-radio-clear">clear</span></label>
						<div>
							<textarea name="i_nhivm_19b" class="form-control"></textarea>
						</div>
					</div>
				@endif
			</div>
		@endif

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>19.c. How have other things like housing, transportation, employment,  insurance coverage, or mental health and substance use impacted your ability to access non-HIV medical care? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_nhivm_19c" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>20. Thinking about the barriers we just discussed, can you tell me about any services that might make it <u>easier</u> to access non-HIV medical care? We are particularly interested in learning how these affect your ability to achieve and/or maintain viral suppression. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_nhivm_20" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>20.a. Has your relationship with your provider or a staff person where you receive non-HIV medical services  made it easier for you to access non-HIV medical service? If so, how? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_nhivm_20a" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>20.b. Have there been other things that made it easier for you to access non-HIV medical services that we have not discussed? If so, what are they, and how did they make it easier? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_nhivm_20b" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>21. Thinking of the past year, describe your experiences getting medication for non-HIV related health conditions. <span class="label label-default btn-radio-clear">clear</span></label>
				<div><small>Probes: consider problems getting prescriptions filled, issues with non-HIV related medication (e.g. medication interaction, prioritizing medication, total pill burden, etc.), changes to insurance.</small></div>
				<div>
					<textarea name="i_nhivm_21" class="form-control"></textarea>
				</div>
			</div>
		</div>
	</div>
</div>
