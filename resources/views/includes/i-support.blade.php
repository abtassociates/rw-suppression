<div class="panel panel-default">
	<div class="panel-body">
		<div class="well well-sm text-info"><strong>Now, let us talk about any of the services you receive that are not strictly medical services.  This includes everything from case management and housing assistance to transportation, and food.  It might alos include mental health or substance use services.  I will refer to these services as "support services."</strong></div>

		<div class="row">
			<div class="form-group col-md-6">
				<label>22. What support services are you currently receiving? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_nhivm_22" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>23. What support services are you currently <u>not receiving</u> that would be helpful to you if you could have them? <span class="label label-default btn-radio-clear">clear</span></label>
				<p><small>Probe: consider how other medical services affect the ability to achieve or maintain viral suppression?</small></p>
				<div>
					<textarea name="i_nhivm_23" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>24. Which support services do you feel are <u>most important</u> in helping people manage HIV? <span class="label label-default btn-radio-clear">clear</span></label>
				<p><small>Probe: explain how these services are important to managing your HIV… or give an example of a time when x service helped you better manage your HIV (e.g., through accessing HIV care, treatment adherence, etc.)?</small></p>
				<div>
					<textarea name="i_nhivm_24" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>25. What are some common challenges to accessing support services? <span class="label label-default btn-radio-clear">clear</span></label>
				<p><small>Probes: consider cost/financing/insurance, application process/getting into the system, confidentiality, availability, housing instability, stigma, mental health, substance use (please follow up with specifics).</small></p>
				<div>
					<textarea name="i_nhivm_25" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>26. What are some things that make it easier to access support services? <span class="label label-default btn-radio-clear">clear</span></label>
				<p><small>Probe: consider proximity/transportation, stable housing, financial aid, etc.</small></p>
				<div>
					<textarea name="i_nhivm_26" class="form-control"></textarea>
				</div>
			</div>
		</div>
	</div>
</div>