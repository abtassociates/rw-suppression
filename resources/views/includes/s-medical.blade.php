<div class="panel panel-default">
	<div class="panel-body">
		<div class="well well-sm text-info">
			<p><strong>Now I am going to make some statements related to the trustworthiness of health care organizations. This could include any place you have received health care services. Please rate your level of agreement with these statements using this agreement scale:</strong></p>

			<p><em>Interviewer note: Please use response card D to answer the following questions.</em></p>

			<table class="table table-bordered" style="width:inherit;">
				<tr class="info">
					<th style="width:20%">0</th>
					<th style="width:20%">1</th>
					<th style="width:20%">2</th>
					<th style="width:20%">3</th>
					<th style="width:20%">4</th>
				</tr>
				<tr>
					<td>Refused to answer</td>
					<td>Strongly Disagree</td>
					<td>Disagree</td>
					<td>Agree</td>
					<td>Strongly Agree</td>
				</tr>
			</table>
		</div>

		<div class="row">
			<div class="form-group col-md-4">
				<div><label>a. You should be cautious when dealing with health care organizations. <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_med_a" value="1"> Strongly Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_a" value="2"> Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_a" value="3"> Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_a" value="4"> Strongly Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_a" value="0"> Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<div><label>b. Clients have sometimes been deceived or misled by health care organizations. <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_med_b" value="1"> Strongly Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_b" value="2"> Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_b" value="3"> Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_b" value="4"> Strongly Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_b" value="0"> Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<div><label>c. When health care organizations make mistakes they usually cover it up. <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_med_c" value="1"> Strongly Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_c" value="2"> Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_c" value="3"> Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_c" value="4"> Strongly Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_c" value="0"> Refused to answer
					</label>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-4">
				<div><label>d. Health care organizations have sometimes done harmful experiments on clients without their knowledge. <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_med_d" value="1"> Strongly Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_d" value="2"> Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_d" value="3"> Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_d" value="4"> Strongly Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_d" value="0"> Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<div><label>e. Health care organizations do not always keep your information totally private. <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_med_e" value="1"> Strongly Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_e" value="2"> Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_e" value="3"> Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_e" value="4"> Strongly Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_e" value="0"> Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<div><label>f. Sometimes I wonder if health care organizations really know what they are doing. <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_med_f" value="1"> Strongly Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_f" value="2"> Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_f" value="3"> Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_f" value="4"> Strongly Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_f" value="0"> Refused to answer
					</label>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-4">
				<div><label>g. Mistakes are common in health care organizations. <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_med_g" value="1"> Strongly Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_g" value="2"> Disagree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_g" value="3"> Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_g" value="4"> Strongly Agree
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_med_g" value="0"> Refused to answer
					</label>
				</div>
			</div>
		</div>
	</div>
</div>
