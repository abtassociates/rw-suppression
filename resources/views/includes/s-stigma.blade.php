<div class="panel panel-default">
	<div class="panel-body">
		<div class="well well-sm text-info">
			<p><strong>Now I am going to ask you some questions about how you may have felt or worried about things like disapproval or judgement from others related to your HIV status. Please answer each one of these questions by selecting the number of the answer that most closely reflects your experience:</strong></p>

			<p><em>Interviewer note: Please use response card C to answer the following questions.</em></p>

			<table class="table table-bordered" style="width:inherit;">
				<tr class="info">
					<th style="width:16%">0</th>
					<th style="width:16%">1</th>
					<th style="width:16%">2</th>
					<th style="width:16%">3</th>
					<th style="width:16%">4</th>
					<th style="width:16%">5</th>
				</tr>
				<tr>
					<td>Refused to answer</td>
					<td>Never</td>
					<td>Rarely</td>
					<td>Sometimes</td>
					<td>Often</td>
					<td>Always</td>
				</tr>
			</table>
		</div>

		<div class="row">
			<div class="form-group col-sm-12">
				<label>1. In the past year have you ever:</label>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-4">
				<div><label>a. Felt having HIV was a punishment for things you had done in the past <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_stg_1a" value="1"> Never
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1a" value="2"> Rarely
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1a" value="3"> Sometimes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1a" value="4"> Often
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1a" value="5"> Always
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1a" value="0"> Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<div><label>b. Felt that people were avoiding you because of your HIV status <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_stg_1b" value="1"> Never
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1b" value="2"> Rarely
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1b" value="3"> Sometimes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1b" value="4"> Often
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1b" value="5"> Always
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1b" value="0"> Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<div><label>c. Feared that you would lose your friends if they learned about your HIV status <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_stg_1c" value="1"> Never
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1c" value="2"> Rarely
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1c" value="3"> Sometimes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1c" value="4"> Often
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1c" value="5"> Always
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1c" value="0"> Refused to answer
					</label>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-4">
				<div><label>d. Felt like people that you know were treating you differently because of your HIV status <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_stg_1d" value="1"> Never
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1d" value="2"> Rarely
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1d" value="3"> Sometimes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1d" value="4"> Often
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1d" value="5"> Always
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1d" value="0"> Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<div><label>e. Felt like people looked down on you because you have HIV <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_stg_1e" value="1"> Never
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1e" value="2"> Rarely
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1e" value="3"> Sometimes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1e" value="4"> Often
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1e" value="5"> Always
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1e" value="0"> Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<div><label>f. Avoided dating because you believed most people do not want a relationship with someone with HIV <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_stg_1f" value="1"> Never
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1f" value="2"> Rarely
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1f" value="3"> Sometimes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1f" value="4"> Often
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1f" value="5"> Always
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1f" value="0"> Refused to answer
					</label>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-4">
				<div><label>g. Avoided a situation because you were worried about people knowing you have HIV <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_stg_1g" value="1"> Never
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1g" value="2"> Rarely
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1g" value="3"> Sometimes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1g" value="4"> Often
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1g" value="5"> Always
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1g" value="0"> Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<div><label>h. Been embarrassed about having HIV <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_stg_1h" value="1"> Never
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1h" value="2"> Rarely
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1h" value="3"> Sometimes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1h" value="4"> Often
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1h" value="5"> Always
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1h" value="0"> Refused to answer
					</label>
				</div>
			</div>
			<div class="form-group col-md-4">
				<div><label>i. Felt that keeping your HIV status secret was important <span class="label label-default btn-radio-clear">clear</span></label></div>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_stg_1i" value="1"> Never
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1i" value="2"> Rarely
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1i" value="3"> Sometimes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1i" value="4"> Often
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1i" value="5"> Always
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_stg_1i" value="0"> Refused to answer
					</label>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-sm-12">
				<label>2. Since you were diagnosed with HIV has any health care provider (doctors, nurses, clinic staff, etc.):</label>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-4">
				<div><label>a. Acted uncomfortable with you? <span class="label label-default btn-radio-clear cbr">clear</span></label></div>
				<label class="radio-inline" style="margin-top:0;">
					<input type="radio" name="s_stg_2a" value="Yes" class="cbr"> Yes
				</label>
				<label class="radio-inline">
					<input type="radio" name="s_stg_2a" value="No" class="cbr"> No
				</label>
			</div>
			<div class="form-group col-md-4">
				<div><label>b. Treated you as an inferior? <span class="label label-default btn-radio-clear cbr">clear</span></label></div>
				<label class="radio-inline" style="margin-top:0;">
					<input type="radio" name="s_stg_2b" value="Yes" class="cbr"> Yes
				</label>
				<label class="radio-inline">
					<input type="radio" name="s_stg_2b" value="No" class="cbr"> No
				</label>
			</div>
			<div class="form-group col-md-4">
				<div><label>c. Preferred to avoid you? <span class="label label-default btn-radio-clear cbr">clear</span></label></div>
				<label class="radio-inline" style="margin-top:0;">
					<input type="radio" name="s_stg_2c" value="Yes" class="cbr"> Yes
				</label>
				<label class="radio-inline">
					<input type="radio" name="s_stg_2c" value="No" class="cbr"> No
				</label>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-4">
				<div><label>d. Refused you service? <span class="label label-default btn-radio-clear cbr">clear</span></label></div>
				<label class="radio-inline" style="margin-top:0;">
					<input type="radio" name="s_stg_2d" value="Yes" class="cbr"> Yes
				</label>
				<label class="radio-inline">
					<input type="radio" name="s_stg_2d" value="No" class="cbr"> No
				</label>
			</div>
		</div>

		<div class="row hr cbr-hcp" style="display:none;">
			<div class="form-group col-md-4">
				<div><label>3. If yes to A, B, C, or D, has this caused you to avoid HIV care as a result? <span class="label label-default btn-radio-clear">clear</span></label></div>
				<label class="radio-inline" style="margin-top:0;">
					<input type="radio" name="s_stg_3" value="Yes"> Yes
				</label>
				<label class="radio-inline">
					<input type="radio" name="s_stg_3" value="No"> No
				</label>
			</div>
		</div>
	</div>
</div>
