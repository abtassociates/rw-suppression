<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
				<p>Good Morning/Afternoon. Thank you for agreeing to participate in this Ryan White HIV/AIDS Program or "Ryan White Program" client interview. My name is {{ auth()->user()->name }}; I work at Abt Associates, a research and consulting firm based in Cambridge, Massachusetts. Abt has been contracted by HRSA/HAB to conduct a project to learn about client's experiences and opinions about their HIV services.  We are part of a team of interviewers who are meeting with clients and staff in 25 sites like this one around the country.</p>

				<p>We are interested in learning about effective programs and what programs and health care providers can do to assist clients in staying healthy.  In addition, we are interested in particular things like stressful life events and how they may affect you and your ability to use health services and stay healthy.  We are particularly interested in your experience with achieving and maintaining viral suppression.</p>

				<p>You were chosen for this project because you received, or have received, Ryan White HIV/AIDS Program-funded services at [SITE NAME]. The information you provide will be very valuable in helping HAB determine the future direction of services provided under the Ryan White HIV/AIDS Program.</p>

				<p>This survey will take approximately 30 minutes to complete. I will ask you questions about your background, experiences accessing healthcare, experiences receiving healthcare and support services, experiences with others in your family or community, and questions about taking care of yourself.  As a reminder, I will be asking some questions on sensitive topics that could be upsetting for you. You do not have to answer any questions that make you feel uncomfortable and you can stop at any time or let me know if you need to take a break. Do you have any questions for me before we begin?</p>

				<p>I want to start with asking you some general questions about your background.</p>
			</div>
		</div>
	</div>
</div>