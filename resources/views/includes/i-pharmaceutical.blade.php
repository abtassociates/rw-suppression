<div class="panel panel-default">
	<div class="panel-body">
		<div class="well well-sm text-info">
			<p><strong>Now we are going to discuss access to medications for HIV in the past year.</strong></p>

			<p><em>Interviewer - please check question one (1) regarding how long the respondent has been coming to this clinic,  if less than one year then query after each question,  "did  you experience this in this clinic or the last clinic where you got care?"</em></p>
		</div>

		<div class="row">
			<div class="form-group col-md-6">
				<label>11. Describe your experiences in getting your HIV medication. Consider challenges and/or helpful experiences. <span class="label label-default btn-radio-clear">clear</span></label>
				<p><small>Probes: consider problems getting prescriptions filled, changes to what your insurance or AIDS Drug Assistance Program (ADAP) would cover, specific medications, frequency of appointments or medication refills, etc.</small></p>
				<div>
					<textarea name="i_hivp_11" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>11.a. [If problem was identified above:] How was the problem resolved or what solution helped? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivp_11a" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>12. Describe your experience with dosing and pill regimens for ART treatments. Consider challenges and/or helpful experiences. <span class="label label-default btn-radio-clear">clear</span></label>
				<p><small>Probes: consider factors such as pill size and frequency (e.g. daily or twice daily regimens), of medication, change of pill regimens due to side effects, etc.</small></p>
				<div>
					<textarea name="i_hivp_12" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>13. What are some other challenges you have faced when accessing HIV medications? <span class="label label-default btn-radio-clear">clear</span></label>
				<p><small>Probes: consider cost/financing/insurance, confidentiality, availability; housing instability, stigma, substance use, mental health (please ask about specifics).</small></p>
				<div>
					<textarea name="i_hivp_13" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>14. What are some things that make it easier to get HIV medications? <span class="label label-default btn-radio-clear">clear</span></label>
				<p>Probe: consider proximity/transportation, stable housing, financial aid, etc.</p>
				<div>
					<textarea name="i_hivp_14" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>15. Over the past year, have you experienced  any barriers to treatment adherence, or to taking your HIV medications as prescribed by your provider. If yes, please describe. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivp_15" class="form-control"></textarea>
				</div>
			</div>
		</div>

		@if(count($c1r) || count($c2r))
			<div class="row hr">
				@if(count($c1r))
					<div class="form-group col-md-6">
						<label>15.a. You mentioned that in the past year you experienced:</label>
						<div class="alert alert-info" style="margin-bottom:0;">
							<ul>
								@foreach($c1r as $k => $v)
									<li>{{ $c1q[$k] }}: <strong>{{ $c1a[$v] }}</strong></li>
								@endforeach
							</ul>
						</div>
						<label>Could you describe how that/those experience(s) has impacted your ability and/or willingness to adhere to HIV medication as prescribed by your provider? <span class="label label-default btn-radio-clear">clear</span></label>
						<div>
							<textarea name="i_hivp_15a" class="form-control"></textarea>
						</div>
					</div>
				@endif
				@if(count($c2r))
					<div class="form-group col-md-6">
						<label>15.b. You mentioned that since you have had HIV, you experienced:</label>
						<div class="alert alert-info" style="margin-bottom:0;">
							<ul>
								@foreach($c2r as $k => $v)
									<li>{{ $c2q[$k] }} <strong>{{ $v }}</strong></li>
								@endforeach
							</ul>
						</div>
						<label>Could you describe how that experience has impacted your ability and/or willingness to adhere to HIV medication as prescribed by your provider? <span class="label label-default btn-radio-clear">clear</span></label>
						<div>
							<textarea name="i_hivp_15b" class="form-control"></textarea>
						</div>
					</div>
				@endif
			</div>
		@endif

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>15.c. Have any other things like housing, transportation, employment, and insurance coverage impacted your ability to adhere to HIV medication as prescribed by your provider?  If yes, please describe. <span class="label label-default btn-radio-clear">clear</span></label>
				<div><small>Probe: Of those you have discussed, which are the most critical or important in your ability to adhere to HIV medication as prescribed by your provider?</small></div>
				<div>
					<textarea name="i_hivp_15c" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>15.d. Have other things like alcohol or substance use impacted your ability to adhere to HIV medication as prescribed by your provider? If yes, please describe. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivp_15d" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>15.e. Have other things like mental health impacted your ability to adhere to HIV medication as prescribed by your provider? If yes, please describe. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivp_15e" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>16. Can you tell me what things have helped with treatment adherence, or taking your HIV medications as prescribed by your provider. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivp_16" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>16.a. Describe anything your provider or clinic staff have done that helped or supported your ability to take your HIV medication as prescribed by your doctor. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivp_16a" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>16.b. Describe anything your pharmacist has done that helped or supported your ability to take your HIV medication as prescribed by your doctor. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivp_16b" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>16.c. Describe any other things that helped or supported your ability to take your HIV medication as prescribed by your doctor that we have not discussed. <span class="label label-default btn-radio-clear">clear</span></label>
				<p><small>Probe: Could include social support systems or interpersonal relationships.</small></p>
				<div>
					<textarea name="i_hivp_16c" class="form-control"></textarea>
				</div>
			</div>
		</div>
	</div>
</div>
