<div class="panel panel-default">
	<div class="panel-body">
		<div class="well well-sm text-info"><strong>The next set of questions I will ask are about health care coverage.</strong></div>

		<div class="row">
			<div class="form-group col-md-4">
				<label>1. When were you first diagnosed with HIV? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<input type="number" class="form-control" name="s_acc_year_diagnosed" min="0" placeholder="Enter year">
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>2. Do you currently have health care coverage? <span class="label label-default btn-radio-clear br" data-target="br-coverage" data-toggle="0">clear</span></label>
				<div class="radio" style="margin-top:0;">
					<label>
						<input type="radio" name="s_acc_coverage" value="Yes" class="br" data-target="br-coverage" data-toggle="1">
						Yes
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="s_acc_coverage" value="No" class="br" data-target="br-coverage" data-toggle="0">
						No
					</label>
				</div>
			</div>
		</div>
		<div class="row hr br-coverage" style="display:none;">
			<div class="form-group col-md-4">
				<label>a. If yes, what type of coverage is it? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<input type="text" class="form-control" name="s_acc_coverage_type">
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>b. How long have you had this health care? <span class="label label-default btn-radio-clear">clear</span></label>
				<div class="row">
					<span class="col-sm-2">Year</span>
					<div class="col-sm-3">
						<input type="number" class="form-control" name="s_acc_coverage_year" min="0">
					</div>
					<span class="col-sm-2 col-sm-offset-1">Month</span>
					<div class="col-sm-3">
						<input type="number" class="form-control" name="s_acc_coverage_month" min="0">
					</div>
				</div>
			</div>
		</div>
		<div class="row hr br-coverage" style="display:none;">
			<div class="form-group col-md-4">
				<label>3. If yes, has this health care coverage helped you access HIV care and medication? If so, how? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea class="form-control" name="s_acc_helped"></textarea>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>4. If yes, has this health care coverage caused any problems for you in getting HIV care and medication? If so, how? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea class="form-control" name="s_acc_problematic"></textarea>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label>5. If yes, have you had any interruptions in your health coverage in the past year? If yes, please describe the situation or circumstances leading to your interruption. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea class="form-control" name="s_acc_interrupted"></textarea>
				</div>
			</div>
		</div>
	</div>
</div>
