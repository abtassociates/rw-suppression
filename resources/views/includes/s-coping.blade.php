<div class="panel panel-default">
    <div class="panel-body">
        <div class="well well-sm text-info">
            <p><strong>For the last set of questions, I am going to ask you some questions about your confidence in your coping skills.</strong></p>

            <p><em>Interviewer note: Please use response card F to answer the below questions.</em></p>

            <table class="table table-bordered" style="width:inherit;">
                <tr class="info">
                    <th style="width:16%">1</th>
                    <th style="width:16%">2</th>
                    <th style="width:16%">3</th>
                    <th style="width:16%">4</th>
                    <th style="width:16%">97</th>
                    <th style="width:16%">98</th>
                </tr>
                <tr>
                    <td>Not at all</td>
                    <td>Used somewhat</td>
                    <td>Used quite a bit</td>
                    <td>Used a great deal</td>
                    <td>Refused to answer</td>
                    <td>Don't know</td>
                </tr>
            </table>
        </div>

        <div class="row">
            <div class="form-group col-sm-12">
                <label>Since (12-MO DATE):</label>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-4">
                <div><label>1a. I knew what had to be done, so I doubled my efforts to make things work. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1a" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1a" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1a" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1a" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1a" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1a" value="98"> Don't know
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>1b. I made a plan of action and followed it. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1b" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1b" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1b" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1b" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1b" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1b" value="98"> Don't know
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>1c. Just concentrated on what I had to do next – the next step. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1c" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1c" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1c" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1c" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1c" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1c" value="98"> Don't know
                    </label>
                </div>
            </div>
        </div>

        <div class="row hr">
            <div class="form-group col-md-4">
                <div><label>1d. Changed something so things would turn out all right. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1d" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1d" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1d" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1d" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1d" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1d" value="98"> Don't know
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>1e. Drew on my past experiences; I was in a similar position before. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1e" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1e" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1e" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1e" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1e" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1e" value="98"> Don't know
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>1f. Came up with a couple of different solutions to the problem. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1f" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1f" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1f" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1f" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1f" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1f" value="98"> Don't know
                    </label>
                </div>
            </div>
        </div>

        <div class="row hr">
            <div class="form-group col-md-4">
                <div><label>1g. Changed or grew as a person in a good way. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1g" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1g" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1g" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1g" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1g" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1g" value="98"> Don't know
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>1h. I came out of the experience better than when I went in. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1h" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1h" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1h" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1h" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1h" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1h" value="98"> Don't know
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>1i. Found new faith. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1i" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1i" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1i" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1i" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1i" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1i" value="98"> Don't know
                    </label>
                </div>
            </div>
        </div>

        <div class="row hr">
            <div class="form-group col-md-4">
                <div><label>1j. Rediscovered what is important in life. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1j" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1j" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1j" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1j" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1j" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1j" value="98"> Don't know
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>1k. I prayed. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1k" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1k" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1k" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1k" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1k" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1k" value="98"> Don't know
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>1l. I changed something about myself. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1l" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1l" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1l" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1l" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1l" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1l" value="98"> Don't know
                    </label>
                </div>
            </div>
        </div>

        <div class="row hr">
            <div class="form-group col-md-4">
                <div><label>1m. I was inspired to do something creative. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cop_1m" value="1"> Not at all
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1m" value="2"> Used somewhat
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1m" value="3"> Used quite a bit
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1m" value="4"> Used a great deal
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1m" value="97"> Refused to answer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cop_1m" value="98"> Don't know
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
