<div class="panel panel-default">
    <div class="panel-body">
        <div class="well well-sm text-info">
            <p><strong>The next few questions I will ask are about taking your HIV medications. I will give you several answer choices for each question. Please select the answer that most accurately represents your experience.</strong></p>

            <p><em>(Directions from tool: Please ask each question and circle the corresponding number next to the answer, then add up the numbers circled to calculate Index score.)</em></p>
        </div>

        <div class="row">
            <div class="form-group col-md-4">
                <div><label>1. How often do you feel that you have difficulty taking your HIV medications on time? By "on time" I mean no more than two hours before or two hours after the time your doctor told you to take it. <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cas_1" value="1"> All the time
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_1" value="2"> Most of the time
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_1" value="3"> Rarely
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_1" value="4"> Never
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_1" value="97"> Refused to answer
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>2. Thinking of the last 3 months, how often would you say that you missed at least one dose of your HIV medications? <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cas_2" value="1"> Everyday
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_2" value="2"> 4-6 days/week
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_2" value="3"> 2-3 days/week
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_2" value="4"> Once a week
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_2" value="5"> Less than once a week
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_2" value="6"> Never
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_2" value="97"> Refused to answer
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>3. When was the last time you missed at least one dose of your HIV medications? <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_cas_3" value="1"> Within the past week
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_3" value="2"> 1-2 weeks ago
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_3" value="3"> 3-4 weeks ago
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_3" value="4"> Between 1 and 3 months ago
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_3" value="5"> More than 3 months ago
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_3" value="6"> Never
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_cas_3" value="97"> Refused to answer
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
