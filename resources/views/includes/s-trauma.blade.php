<div class="panel panel-default">
	<div class="panel-body">
		<div class="well well-sm text-info">
			<p><strong>In the next section of the interview, I will ask questions about events that sometimes change people's lives, like the death of a friend or family member, illness or injuries, and employment or relationship issues, as well as other events. Some of the questions are very personal and ask about sensitive topics that could be upsetting to you. Please take as much time as you need to answer each question. Also, please remember that everything you tell me is confidential and you do not have to answer any questions you are not comfortable answering. You can stop at any time.</strong></p>

			<p><em>Interviewer note: Please use response card E to answer the following questions.</em></p>

			<table class="table table-bordered" style="width:inherit;">
				<tr class="info">
					<th style="width:14%">1</th>
					<th style="width:14%">2</th>
					<th style="width:14%">3</th>
					<th style="width:14%">4</th>
					<th style="width:14%">5</th>
					<th style="width:14%">7</th>
					<th style="width:14%">8</th>
				</tr>
				<tr>
					<td>Extremely stressful</td>
					<td>Very stressful</td>
					<td>Moderately stressful</td>
					<td>A little stressful</td>
					<td>Not stressful</td>
					<td>Refused to answer</td>
					<td>Don't know</td>
				</tr>
			</table>

			<table class="table table-bordered" style="width:inherit;">
				<tr class="info">
					<th style="width:25%">1</th>
					<th style="width:25%">2</th>
					<th style="width:25%">7</th>
					<th style="width:25%">8</th>
				</tr>
				<tr>
					<td>Yes</td>
					<td>No</td>
					<td>Refused to answer</td>
					<td>Don't know</td>
				</tr>
			</table>
		</div>

		<div class="row">
			<div class="form-group col-sm-12">
				<label>1. First I am going to ask you about events in your relationships. Since (12-MO/DATE), have you</label>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div><label>a. gotten married, engaged or made a formal commitment to a partner, including a ceremony <span class="label label-default btn-radio-clear br" data-target="s_tra_1ai" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_1a" value="1" class="br" data-target="s_tra_1ai" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1a" value="2" class="br" data-target="s_tra_1ai" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1a" value="7" class="br" data-target="s_tra_1ai" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1a" value="8" class="br" data-target="s_tra_1ai" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_1ai" style="display:none;">
					<div><label>i. If yes, when it occurred, how would you rate your stress using the scale? <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_1ai" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1ai" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1ai" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1ai" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1ai" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1ai" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1ai" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div><label>b. gotten divorced, separated or had a break-up with a partner (mate/girlfriend/boyfriend) (must have been in the committed relationship at least 6 months) <span class="label label-default btn-radio-clear br" data-target="s_tra_1bi" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_1b" value="1" class="br" data-target="s_tra_1bi" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1b" value="2" class="br" data-target="s_tra_1bi" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1b" value="7" class="br" data-target="s_tra_1bi" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1b" value="8" class="br" data-target="s_tra_1bi" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_1bi" style="display:none;">
					<div><label>i. If yes, when it occurred, how would you rate your stress using the scale? <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_1bi" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1bi" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1bi" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1bi" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1bi" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1bi" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_1bi" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="col-md-4">
				<div class="form-group">
					<div><label>2. Now, I am going to ask you about any experiences with death of people who are very close to you. Since (DATE), have you experienced the death of a close family member or very close friend? <span class="label label-default btn-radio-clear br" data-target="s_tra_2i" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_2" value="1" class="br" data-target="s_tra_2i" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_2" value="2" class="br" data-target="s_tra_2i" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_2" value="7" class="br" data-target="s_tra_2i" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_2" value="8" class="br" data-target="s_tra_2i" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_2i" style="display:none;">
					<div><label>i. If yes, when it occurred, how would you rate your stress using the scale? <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_2i" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_2i" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_2i" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_2i" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_2i" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_2i" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_2i" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div><label>3. Since (12-MO DATE), have any close family members or very close friends experienced a serious illness or injury? (Please do not include those whom you have mentioned that died.) <span class="label label-default btn-radio-clear br" data-target="s_tra_3i" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_3" value="1" class="br" data-target="s_tra_3i" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_3" value="2" class="br" data-target="s_tra_3i" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_3" value="7" class="br" data-target="s_tra_3i" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_3" value="8" class="br" data-target="s_tra_3i" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_3i" style="display:none;">
					<div><label>i. If yes, when it occurred, how would you rate your stress using the scale? <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_3i" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_3i" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_3i" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_3i" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_3i" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_3i" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_3i" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="well well-sm text-info">
			Now, I am going to ask you about problems with work and finances
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div><label>4. Since (12-MO DATE), have you been employed at any time? <span class="label label-default btn-radio-clear br" data-target="s_tra_4a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_4" value="1" class="br" data-target="s_tra_4a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4" value="2" class="br" data-target="s_tra_4a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4" value="7" class="br" data-target="s_tra_4a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4" value="8" class="br" data-target="s_tra_4a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_4a" style="display:none;">
					<div><label>a. (If yes). Since (DATE) , have you had trouble with your employer such as being in danger of losing your job, being suspended or demoted, experiencing discrimination, or any other major problems with your job? <span class="label label-default btn-radio-clear br" data-target="s_tra_4b" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_4a" value="1" class="br" data-target="s_tra_4b" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4a" value="2" class="br" data-target="s_tra_4b" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4a" value="7" class="br" data-target="s_tra_4b" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4a" value="8" class="br" data-target="s_tra_4b" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_4b" style="display:none;">
					<div><label>b. (If yes) When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_4b" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4b" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4b" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4b" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4b" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4b" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_4b" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div><label>5. Since (12-MO DATE), have you lost your job, that is: you were you fired, laid off, quit, or retired? <span class="label label-default btn-radio-clear br" data-target="s_tra_5a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_5" value="1" class="br" data-target="s_tra_5a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_5" value="2" class="br" data-target="s_tra_5a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_5" value="7" class="br" data-target="s_tra_5a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_5" value="8" class="br" data-target="s_tra_5a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_5a" style="display:none;">
					<div><label>a. (If yes) When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_5a" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_5a" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_5a" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_5a" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_5a" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_5a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_5a" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div><label>6. Since (12-MO DATE), have you been out of work for at least 2 months? <span class="label label-default btn-radio-clear br" data-target="s_tra_6ab" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_6" value="1" class="br" data-target="s_tra_6ab" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6" value="2" class="br" data-target="s_tra_6ab" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6" value="7" class="br" data-target="s_tra_6ab" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6" value="8" class="br" data-target="s_tra_6ab" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_6ab" style="display:none;">
					<div><label>a. (If yes) During that time while you were out of a job, were you seriously looking for a job? <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_6a" value="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6a" value="2"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6a" value="8"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_6ab" style="display:none;">
					<div><label>b. (If yes) How stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_6b" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6b" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6b" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6b" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6b" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6b" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_6b" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="col-md-4">
				<div class="form-group">
					<div><label>7. Since (12-MO DATE), did you experience any financial problems? <span class="label label-default btn-radio-clear br" data-target="s_tra_7a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_7" value="1" class="br" data-target="s_tra_7a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_7" value="2" class="br" data-target="s_tra_7a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_7" value="7" class="br" data-target="s_tra_7a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_7" value="8" class="br" data-target="s_tra_7a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_7a" style="display:none;">
					<div><label>a. If yes, when it occurred, how would you rate your stress using the scale? <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_7a" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_7a" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_7a" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_7a" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_7a" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_7a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_7a" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div><label>8. Since (12-MO DATE), have you or a partner become pregnant, had a baby or adopted a baby? <span class="label label-default btn-radio-clear br" data-target="s_tra_8a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_8" value="1" class="br" data-target="s_tra_8a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_8" value="2" class="br" data-target="s_tra_8a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_8" value="7" class="br" data-target="s_tra_8a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_8" value="8" class="br" data-target="s_tra_8a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_8a" style="display:none;">
					<div><label>a. (If yes) When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_8a" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_8a" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_8a" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_8a" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_8a" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_8a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_8a" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="well well-sm text-info">
			Now, I am going to ask you about illness, accidents, and injuries. As a reminder, all your answers will be kept confidential and you may skip any question you do not wish to answer. You may also end the interview at any point.
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div><label>9. Since (12-MO DATE), besides illness related to HIV infection, have you had a major illness, chronic health problem or injury? <span class="label label-default btn-radio-clear br" data-target="s_tra_9ab" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_9" value="1" class="br" data-target="s_tra_9ab" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9" value="2" class="br" data-target="s_tra_9ab" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9" value="7" class="br" data-target="s_tra_9ab" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9" value="8" class="br" data-target="s_tra_9ab" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_9ab" style="display:none;">
					<div><label>a. Were you hospitalized? <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_9a" value="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9a" value="2"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9a" value="8"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_9ab" style="display:none;">
					<div><label>b. When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_9b" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9b" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9b" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9b" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9b" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9b" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_9b" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div><label>10. Since (12-MO DATE), were you physically attacked or assaulted or had your life threatened? <span class="label label-default btn-radio-clear br" data-target="s_tra_10a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_10" value="1" class="br" data-target="s_tra_10a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_10" value="2" class="br" data-target="s_tra_10a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_10" value="7" class="br" data-target="s_tra_10a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_10" value="8" class="br" data-target="s_tra_10a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_10a" style="display:none;">
					<div><label>a. (If yes) When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_10a" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_10a" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_10a" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_10a" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_10a" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_10a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_10a" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div><label>11. Since (12-MO DATE), were you sexually abused or assaulted? That is, did anyone touch your sexual organs (breasts, penis, vagina, anus, etc.) or make you touch their sexual organs by using force or threatening to harm you? <span class="label label-default btn-radio-clear br" data-target="s_tra_11a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_11" value="1" class="br" data-target="s_tra_11a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_11" value="2" class="br" data-target="s_tra_11a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_11" value="7" class="br" data-target="s_tra_11a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_11" value="8" class="br" data-target="s_tra_11a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_11a" style="display:none;">
					<div><label>a. (If yes) When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_11a" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_11a" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_11a" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_11a" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_11a" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_11a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_11a" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="col-md-4">
				<div class="form-group">
					<div><label>12. Since (12-MO DATE), have you felt unsafe in your neighborhood? <span class="label label-default btn-radio-clear br" data-target="s_tra_12a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_12" value="1" class="br" data-target="s_tra_12a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_12" value="2" class="br" data-target="s_tra_12a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_12" value="7" class="br" data-target="s_tra_12a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_12" value="8" class="br" data-target="s_tra_12a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_12a" style="display:none;">
					<div><label>a. (If yes) When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_12a" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_12a" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_12a" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_12a" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_12a" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_12a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_12a" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="well well-sm text-info">
			Now I will be asking you about any history of interactions with law enforcement/police.
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div><label>13. Since (12-MO DATE), have you been arrested for a serious crime such as, driving under the influence of alcohol or drugs, robbery, drugs, or a crime involving more than just a fine? <span class="label label-default btn-radio-clear br" data-target="s_tra_13a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_13" value="1" class="br" data-target="s_tra_13a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_13" value="2" class="br" data-target="s_tra_13a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_13" value="7" class="br" data-target="s_tra_13a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_13" value="8" class="br" data-target="s_tra_13a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_13a" style="display:none;">
					<div><label>a. When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_13a" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_13a" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_13a" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_13a" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_13a" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_13a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_13a" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div><label>14. Since (12-MO DATE), were you convicted of a crime and sent to jail or prison? <span class="label label-default btn-radio-clear br" data-target="s_tra_14ab" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_14" value="1" class="br" data-target="s_tra_14ab" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_14" value="2" class="br" data-target="s_tra_14ab" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_14" value="7" class="br" data-target="s_tra_14ab" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_14" value="8" class="br" data-target="s_tra_14ab" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_14ab" style="display:none;">
					<div><label>a. (If yes) How many days were you in jail or prison? <span class="label label-default btn-radio-clear">clear</span></label></div>
					<input type="number" class="form-control" name="s_tra_14a" min="1" max="270">
				</div>

				<div class="form-group s_tra_14ab" style="display:none;">
					<div><label>b. When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_14b" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_14b" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_14b" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_14b" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_14b" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_14b" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_14b" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div><label>15. Since (12-MO DATE), was a friend or close relative arrested for a serious crime, with likely jail or prison sentence, or sent to jail or prison for at least one month? <span class="label label-default btn-radio-clear br" data-target="s_tra_15a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_15" value="1" class="br" data-target="s_tra_15a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_15" value="2" class="br" data-target="s_tra_15a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_15" value="7" class="br" data-target="s_tra_15a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_15" value="8" class="br" data-target="s_tra_15a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_15a" style="display:none;">
					<div><label>a. When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_15a" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_15a" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_15a" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_15a" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_15a" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_15a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_15a" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="col-md-4">
				<div class="form-group">
					<div><label>16. Since (12-MO DATE), were you robbed or was your home burglarized? <span class="label label-default btn-radio-clear br" data-target="s_tra_16a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_16" value="1" class="br" data-target="s_tra_16a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_16" value="2" class="br" data-target="s_tra_16a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_16" value="7" class="br" data-target="s_tra_16a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_16" value="8" class="br" data-target="s_tra_16a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_16a" style="display:none;">
					<div><label>a. When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_16a" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_16a" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_16a" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_16a" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_16a" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_16a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_16a" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div><label>17. Since (12-MO DATE), did you move your residence more than once? <span class="label label-default btn-radio-clear br" data-target="s_tra_17a" data-toggle="0">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_17" value="1" class="br" data-target="s_tra_17a" data-toggle="1"> Yes
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_17" value="2" class="br" data-target="s_tra_17a" data-toggle="0"> No
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_17" value="7" class="br" data-target="s_tra_17a" data-toggle="0"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_17" value="8" class="br" data-target="s_tra_17a" data-toggle="0"> Don't know
						</label>
					</div>
				</div>

				<div class="form-group s_tra_17a" style="display:none;">
					<div><label>a. (If yes) When it occurred, how stressful or difficult was this? Would you say it was <span class="label label-default btn-radio-clear">clear</span></label></div>
					<div class="radio" style="margin-top:0;">
						<label>
							<input type="radio" name="s_tra_17a" value="1"> Extremely stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_17a" value="2"> Very stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_17a" value="3"> Moderately stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_17a" value="4"> A little stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_17a" value="5"> Not stressful
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_17a" value="7"> Refused to answer
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="s_tra_17a" value="8"> Don't know
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
