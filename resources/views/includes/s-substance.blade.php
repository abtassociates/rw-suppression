<div class="panel panel-default">
    <div class="panel-body">
        <div class="well well-sm text-info">
            <strong>Now I would like to ask you about experiences related to alcohol or drug use that you have had in the past 12 months.</strong>
        </div>

        <div class="row">
            <div class="form-group col-md-4">
                <div><label>1. Since (12-MO DATE), have you drunk an alcoholic beverage (wine, beer, malt beverage, liquor) and/or used drugs (including non-medical use of prescription drugs)? <span class="label label-default btn-radio-clear br" data-target="s_sub_2" data-toggle="0">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_sub_1" value="Yes" class="br" data-target="s_sub_2" data-toggle="1"> Yes
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_1" value="No" class="br" data-target="s_sub_2" data-toggle="0"> No
                    </label>
                </div>
            </div>
        </div>

        <div class="row hr s_sub_2" style="display:none;">
            <div class="form-group col-md-4">
                <div><label>2a. Have you spent more time drinking or using drugs than you intended? <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_sub_2a" value="Yes"> Yes
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2a" value="No"> No
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2a" value="Don't Know"> Don't Know
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2a" value="Refused to Answer"> Refused to Answer
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>2b. Have you neglected some of your usual responsibilities because of alcohol or drug use? <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_sub_2b" value="Yes"> Yes
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2b" value="No"> No
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2b" value="Don't Know"> Don't Know
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2b" value="Refused to Answer"> Refused to Answer
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>2c. Have you wanted to cut down on your drinking or drug use? <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_sub_2c" value="Yes"> Yes
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2c" value="No"> No
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2c" value="Don't Know"> Don't Know
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2c" value="Refused to Answer"> Refused to Answer
                    </label>
                </div>
            </div>
        </div>

        <div class="row hr s_sub_2" style="display:none;">
            <div class="form-group col-md-4">
                <div><label>2d. Has anyone objected to your use of alcohol or drug use? <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_sub_2d" value="Yes"> Yes
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2d" value="No"> No
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2d" value="Don't Know"> Don't Know
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2d" value="Refused to Answer"> Refused to Answer
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>2e. Have you frequently found yourself thinking about drinking or drug use? <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_sub_2e" value="Yes"> Yes
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2e" value="No"> No
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2e" value="Don't Know"> Don't Know
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2e" value="Refused to Answer"> Refused to Answer
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div><label>2f. Have you used alcohol or drugs to relieve feelings such as sadness, anger, or boredom? <span class="label label-default btn-radio-clear">clear</span></label></div>
                <div class="radio" style="margin-top: 0;">
                    <label>
                        <input type="radio" name="s_sub_2f" value="Yes"> Yes
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2f" value="No"> No
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2f" value="Don't Know"> Don't Know
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="s_sub_2f" value="Refused to Answer"> Refused to Answer
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
