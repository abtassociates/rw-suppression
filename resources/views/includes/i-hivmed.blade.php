<div class="panel panel-default">
	<div class="panel-body">
		<div class="well well-sm text-info"><strong>These questions will focus on <u>HIV medical services</u>.  HIV medical services might include office visits for check-ups with your doctor (or other clinician e.g. nurse practitioner, physician assistant), receiving prescription medication, or lab tests like viral loads and CD4 tests.</strong></div>

		<div class="row">
			<div class="form-group col-md-6">
				<label>1. Please tell me how long it has been since you were diagnosed with HIV. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<input type="number" class="form-control" name="i_hivm_1" min="0">
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>2. How long have you been coming to this clinic? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<input type="number" class="form-control" name="i_hivm_2" min="0">
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>3. Please describe the types of HIV medical services you regularly access here or elsewhere. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivm_3" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>4. How often do you receive these services? <span class="label label-default btn-radio-clear">clear</span></label>
				<div><small># of times per month or # of times per year</small></div>
				<div>
					<input type="text" name="i_hivm_4" class="form-control">
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>5. Which of these HIV medical services do you receive at this clinic? <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivm_5" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>6. Thinking about the last year, which HIV medical services do you feel were <u>most important</u> in helping you manage your HIV? Why? <span class="label label-default btn-radio-clear">clear</span></label>
				<div><small>Probe: How do these medical services affect your ability to maintain viral suppression?</small></div>
				<div>
					<textarea name="i_hivm_6" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>7. Again thinking of the past year, what are some challenges you have faced getting HIV medical services? This can include services at this clinic or services you received before you started coming to this clinic.  We are particularly interested in hearing about your experiences <u>in the past year</u> and how these barriers affect your ability to be virally suppressed. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivm_7" class="form-control"></textarea>
				</div>
			</div>
		</div>

		@if(count($c1r) || count($c2r))
			<div class="row hr">
				@if(count($c1r))
					<div class="form-group col-md-6">
						<label>7.a. You mentioned that in the past year you experienced:</label>
						<div class="alert alert-info" style="margin-bottom:0;">
							<ul>
								@foreach($c1r as $k => $v)
									<li>{{ $c1q[$k] }}: <strong>{{ $c1a[$v] }}</strong></li>
								@endforeach
							</ul>
						</div>
						<label>Could you describe how that/those experience(s) has impacted your ability and/or willingness to get HIV medical services? <span class="label label-default btn-radio-clear">clear</span></label>
						<div>
							<textarea name="i_hivm_7a" class="form-control"></textarea>
						</div>
					</div>
				@endif
				@if(count($c2r))
					<div class="form-group col-md-6">
						<label>7.b.You mentioned that since you have had HIV, you experienced:</label>
						<div class="alert alert-info" style="margin-bottom:0;">
							<ul>
								@foreach($c2r as $k => $v)
									<li>{{ $c2q[$k] }} <strong>{{ $v }}</strong></li>
								@endforeach
							</ul>
						</div>
						<label>Could you describe how that experience has impacted your ability and/or willingness to access HIV medical services? <span class="label label-default btn-radio-clear">clear</span></label>
						<div>
							<textarea name="i_hivm_7b" class="form-control"></textarea>
						</div>
					</div>
				@endif
			</div>
		@endif

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>7.c. Have other things like housing, transportation, employment, or health care coverage impacted your ability to access HIV medical services?  If so, how? <span class="label label-default btn-radio-clear">clear</span></label>
				<div><small>Probe: Of those you have discussed, which are the most critical or important in your ability to access HIV medical services?</small></div>
				<div>
					<textarea name="i_hivm_7c" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>7.d. Has alcohol or drug use (including non-medical use of prescription drugs) impacted your ability to access HIV medical services? If yes, please explain. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivm_7d" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>7.e. Have mental health challenges impacted your ability to access HIV medical services? If yes, please explain. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivm_7e" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>8. Have there been any  programs or services that helped you become more engaged in care or return to care?  If yes, please describe the program/service and how it helped you. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivm_8" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label>9. Do you think there are any specific things that a clinic can do to create an environment that helps you get and keep your viral load as low as possible? Please provide examples. <span class="label label-default btn-radio-clear">clear</span></label>
				<div><small>Probe: appointment availability, separate clinic, separate space, clinic staff that are reflective of community, cultural competency (explain further), incentives, etc.)?</small></div>
				<div>
					<textarea name="i_hivm_9" class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="row hr">
			<div class="form-group col-md-6">
				<label>10. Do you think there are any specific things that your provider (physician, doctor, nurse) can do to help you get and keep your viral load as low as possible? Please provide examples. <span class="label label-default btn-radio-clear">clear</span></label>
				<div>
					<textarea name="i_hivm_10" class="form-control"></textarea>
				</div>
			</div>
		</div>
	</div>
</div>