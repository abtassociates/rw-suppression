<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
				<p><em>[Interviewer: ask interviewee if s/he needs to take a break before beginning the client interview]</em></p>

				<p>Thank you for agreeing to continue talking with us today. Now we are going to start the interview part of this discussion, which will be more of conversation about some of the things we talked about earlier. As a reminder, we are interested in things that affect your ability to stay healthy. This includes things like your viral load test numbers that you may get from your doctor, health care coverage, access and use of health services, and stressful life events. We want to understand how these sorts of things might affect your your viral load numbers. Sometimes I will use the term "viral suppression or virally suppressed." By this, I mean having a viral load count below 200.</p>

				<p>Remember, your answers in this discussion are confidential. You can skip any questions that you do not want to answer and you can end the interview at any time. This interview will be audio recorded for note-taking purposes but the recording can be turned off at any point you wish.</p>
			</div>
		</div>
	</div>
</div>