@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <form id="consent" action="{{ route('consent.save', $id) }}" method="post">
            <input name="_method" type="hidden" value="PUT">
            {!! csrf_field() !!}
            <div id="id-strip" class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div id="id-strip-ucid" class="col-sm-12 col-md-7">Unique Client ID:
                            <strong>{{ $euci }}</strong></div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <h4>Client Information Sheet and Consent Form</h4>

                            <p><strong>Consent for semi-structured interview, medical record abstraction and client
                                    interview</strong></p>

                            <p><em>[Interviewer Note: read the consent language below]</em></p>

                            <p>Hello, my name is {{ auth()->user()->name }}.</p>

                            <p><em>[Interviewer Note: read the consent language below]</em></p>

                            <p>Early and ongoing treatment for HIV has been shown to help people living with HIV stay
                                healthy. Also, when a person is effectively treated for their HIV, transmission to an
                                uninfected person is unlikely to occur. However, many people living with HIV still don’t
                                get care regularly. It is important to understand why in order to create more effective
                                programs. To understand why people living with HIV may not get care regularly, the
                                Health Resources Services Administration (HRSA) has funded our company, Abt Associates,
                                to conduct this work. HRSA is the Federal agency that funds the Ryan White HIV/AIDS
                                Program, which funds nearly 2,000 HIV/AIDS service providers like [SITE NAME]. Abt
                                Associated is a private company that conducts evaluations for the Federal
                                government.</p>

                            <p>In this study we are asking people living with HIV about HIV treatment and other care
                                services, as well as things like stressful life events. We are doing this to see how
                                those things may affect people in getting care and staying healthy. If you agree to
                                participate, we will ask you about those topics and how they might affect your level of
                                viral suppression. By "viral suppression" we mean having viral loads below 200. We
                                invite you to participate in this project to help HRSA learn more about your experiences
                                in order to improve the health of people living with HIV.</p>

                            <p>You were chosen for this project because you received, or have received, Ryan White
                                HIV/AIDS Program-funded services at [SITE NAME]. The information you provide will be
                                very valuable in helping HAB determine the future direction of services provided under
                                the Ryan White HIV/AIDS Program.</p>

                            <p>
                                <strong>What will my participation involve?</strong><br>
                                If you decide to participate in the project, you will be asked to consent to participate
                                in a survey administered by an Abt staff member that takes approximately 30-minutes to
                                complete, and an additional interview with the Abt staff member to further discuss
                                barriers and challenges to staying healthy and achieving and/or maintaining viral
                                suppression that takes an additional 60-minutes to complete. Your consent also allows us
                                to get information from your medical records, but not your name from [SITE]. We will ask
                                some questions on sensitive topics that might be upsetting to you. Because it is
                                important to understand your medical history as well as your opinions about barriers and
                                facilitators of care, we are asking for your consent to <u>all three components</u>.
                                Please note however, that you can refuse to answer any questions that you are not
                                comfortable with and end the survey or interview at any time.
                            </p>

                            <p>The Abt interviewer will ask you about your experiences receiving HIV care and treatment,
                                and any problems or barriers you have had in getting HIV care as well as other medical
                                and support services. The interview will include questions about your experiences with
                                your health care providers and experiences with friends or family as it relates to HIV.
                                We will also be reviewing your medical records from [SITE] to learn more about your
                                health care coverage, any medical conditions you have had in the last 18 months and
                                other support or services you may have received. We will not collect any information
                                from your medical record that directly identifies you.</p>

                            <p>We will take written notes on a secure laptop computer. Only members of the project team
                                will have access to those notes. We will also audio record the interview as a backup of
                                our written notes. We will store all the electronic recordings and written notes on a
                                secure server and only members of the Abt team will have access to them. At the end of
                                the contract all interview written notes and audio recordings will be destroyed. Your
                                name will not appear in any of the reports or papers resulting from this project. If at
                                any point you wish to turn the audio recording device off, please let the interviewer
                                know.</p>

                            <p>
                                <strong>Are there any risks?</strong><br>
                                Your participation is voluntary and involves no significant risks to you. Whether or not
                                you participate, it will have no effect on your relationship with [SITE], any other
                                organizations, or the Ryan White HIV/AIDS Program. We will be sharing your responses and
                                medical record data with HRSA, but without any information that can identify you. With
                                any project, there is always a risk of a breach of confidentiality, meaning that other
                                people outside the project team will see the information you provide. However, the
                                project has procedures to protect your confidentiality as described below.
                            </p>

                            <p>
                                <strong>How will you protect my information?</strong><br>
                                The information from your medical records and the information you provide in the
                                interview will be kept confidential. We will only use a coded number attached to your
                                information. No information that directly identifies will be collected by our team.</p>

                            <p>
                                <strong>Are there any benefits?</strong><br>
                                There are no immediate benefits to you. However, we expect that the results will help
                                HAB determine the future directions of the Ryan White HIV/AIDS Program and the services
                                it provides.</p>

                            <p>
                                <strong>Will I be paid for participating?</strong><br>
                                We will provide a $20 gift card to [STORE NAME] to compensate you for your time for
                                agreeing to the medical record review and participating in the survey. Upon completion
                                of the interview, we will provide you with an additional $25 gift card to compensate you
                                for your time.</p>

                            <p>
                                <strong>Who should I contact if I have questions?</strong><br>
                                If you have questions about the project or your rights as a project participant, please
                                feel free to contact the project director, Jane Fox of Abt Associates Inc. at
                                617-349-2873. If you have questions about your rights as a research participant, you may
                                contact Katie Speanburg, the Abt Institutional Review Board Chairperson at this toll
                                free number: (877) 520-6835.</p>

                            <p>
                                <strong>Consent to participate in this project</strong><br>
                                When asked by the interviewer, please provide a verbal "yes" to indicate that you have
                                read and understand the information in this consent form, and voluntarily agree to
                                participate in this project.</p>

                            <p>
                                (See and read addendum for site consent language in paper hard copy form, if any)
                            </p>

                            <div class="row form-group">
                                <div class="col-sm-6 text-right"><label>Interviewer signature on behalf of
                                        participant:</label></div>
                                <div class="col-sm-6"><input type="text" class="form-control"
                                                             name="interviewer_signature" required></div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sm-6 text-right"><label>Date:</label></div>
                                <div class="col-sm-6">
                                    <input type="hidden" name="c_time" value="{{ $timestamp }}">
                                    <input type="text" class="form-control" name="c_time_holder"
                                           value="{{ $timestamp }}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <nav class="form-nav">
                <ul class="pager">
                    <li class="next pull-right"><input type="submit" class="btn btn-primary" name="action"
                                                       value="Submit"></li>
                </ul>
            </nav>
        </form>
    </div>
@endsection
